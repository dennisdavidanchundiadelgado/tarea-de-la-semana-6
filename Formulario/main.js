function validar() {
    var Cedula, nombres, correo, contrasena, estado, genero, ciudad, direccion, telefono ;
    Cedula= document.getElementById("Cedula").value;
    nombres= document.getElementById("nombres").value;
    correo= document.getElementById("correo").value;
    contrasena= document.getElementById("contrasena").value;
    estado= document.getElementById("estado").value;
    genero= document.getElementById("genero").value;
    ciudad= document.getElementById("ciudad").value;
    direccion= document.getElementById("direccion").value;
    telefono= document.getElementById("telefono").value;
    var letras =/^[a-zA-Z\s]*$/;
    var email = /\S+@\S+\.\S+/;

    // Valacion para que cada campo no quede vacio
    if(Cedula === "" || nombres === "" || correo === "" || contrasena === "" ||  estado === ""|| genero === "" || ciudad === "" || direccion === "" || telefono === ""){
        alert("Todos los campos son obligatorios");
        return false;
    }

    // Validacion para que el campo cedula y telefono solo acepte numeros
    if( isNaN(telefono) ) { 
        alert("En el campo telefono solo aceptan numeros"); 
        return false; 
   }if( isNaN(Cedula) ) { 
       alert("En el campo Cedula solo aceptan numeros"); 
       return false; 
}
   // Validacion para que en los campos de texto solo acepte letras

    if(nombres.search(letras)){
        alert("En el campo Nombres Completos solo se aceptan letras ");
        return false;
    }   if(estado.search(letras)){
        alert("En el campo Nombres Completos solo se aceptan letras ");
        return false;
    }   if(genero.search(letras)){
        alert("En el campo Nombres Completos solo se aceptan letras ");
        return false;
    }   if(ciudad.search(letras)){
        alert("En el campo Nombres Completos solo se aceptan letras ");
        return false;
    }   if(direccion.search(letras)){
        alert("En el campo Nombres Completos solo se aceptan letras ");
        return false;
    }
       // Validacion para que en los campos de correo 


    if(correo.search(email))  {
        alert("escriba correctamente el correo");
        return false; 
        } 

    } 
    
// Boton para mostrar las cookies
function getCookie(c_name){
    var i,x,y,ARRcookies=document.cookie.split(";");
    for(i=0; i<ARRcookies.length;i++){
        x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
        y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
        x=x.replace(/^\s+|\s+$/g,"");
        if(x==c_name){
            return unescape(y);
        }
    }
}
function setCookie(c_name, value, exdays){
    var exdate=new Date();
    exdate.setDate(exdate.getDate()+exdays);
    var c_value=escape(value) +((exdays==null) ? "" : "; expires="+exdate.toUTCString());
    document.cookie=c_name+"="+c_value;

}
function checkCookie(){
    var username=getCookie("username");
    if(username!=null && username!=""){
        alert("Bienvenido"+username);
    }else{
        username=prompt("Por favor, Introduzca su usuario:","");
        if(username!=null && username!=""){
            setCookie("username", username,365);
        }
    }
}